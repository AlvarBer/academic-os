#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "mytar.h"

extern char *use;

/** Copy nBytes bytes from the origin file to the destination file.
 *
 * origin: pointer to the FILE descriptor associated with the origin file
 * destination: pointer to FILE descriptor associated with the destination file
 * nBytes: number of bytes to copy
 *
 * Returns the number of bytes actually copied or -1 if an error occurred.
 */
int copynFile(FILE *origin, FILE *destination, int nBytes) {
	int ch, actBytes = 0, error = 0;

	while ((actBytes < nBytes) && !error && ((ch = fgetc(origin)) != EOF)) {
		if (ferror(origin) != 0) {
			error = 0;
		}
		else {
			fputc(ch, destination);
			++actBytes;
			if ((ferror(destination)) != 0) {
				error = 0;
			}
		}
		
	}

	if (error) {
		actBytes = -1;
		clearerr(origin); // We clear stream errors after detecting one 
		clearerr(destination);
	}

	return actBytes;
}

/** Loads a string from a file.
 *
 * file: pointer to the FILE descriptor 
 * buf: parameter to return the read string. Buf is a
 * string passed by reference. 
 * 
 * The loadstr() function must allocate memory from the heap to store 
 * the contents of the string read from the FILE. 
 * Once the string has been properly "built" in memory, return the starting 
 * address of the string (pointer returned by malloc()) in the buf parameter
 * (*buf)=<address>;
 * 
 * Returns: 0 if success, -1 if error
 */
int loadstr(FILE *file, char **buf) {
	int result, success, ch, strSize = 0;
	char *str;

	while ((ch = fgetc(file)) != '\0') { // Obtain string size (The hard way)
		++strSize;
	}
		
	str = malloc(strSize + 1); // We allocate the appropriated size in memory
	
	fseek(file, -(strSize + 1), SEEK_CUR); // Then go back to read the string

	result = fread(str, strSize + 1, 1, file);
	
	(*buf) = str;

	if (result != 1) {
		success = -1;
		clearerr(file);
	}
	else {
		success = 0;
	}

	return success;
}

/** Read tarball header and store it in memory.
 *
 * tarFile: pointer to the tarball's FILE descriptor 
 * header: output parameter. It is used to return the starting memory address
 * of an array that contains the (name,size) pairs read from the tar file
 * nFiles: output parameter. Used to return the number of files stored in
 * the tarball archive (filerst 4 bytes of the header)
 *
 * On success it returns EXIT_SUCCESS. Upon failure, EXIT_FAILURE is returned.
 * (both macros are defilened in stdlib.h).
 */
int readHeader(FILE *tarFile, stHeaderEntry **header, int *nFiles) {
 	stHeaderEntry *array = NULL;
	int error, i = 0;
	
	error = fread(nFiles, sizeof(int), 1, tarFile) == 1 ? 0 : 1;	

	array = malloc(sizeof(stHeaderEntry) * (*nFiles));

	while((i < (*nFiles)) && (!error)) { // Now we read pairs of <name, size>
		error = loadstr(tarFile, (&array[i].name));
		if(!error) {
			error = fread(&(array[i].size),sizeof(int),1,tarFile) == 1 ? 0 : 1;
			++i;
		}
	}

	(*header) = array;
	array = NULL; // Just in case

 	if(error) {
		clearerr(tarFile);
		return EXIT_FAILURE;
	}
	else {
		return EXIT_SUCCESS; 
	}
}

/** Creates a tarball archive 
 *
 * nfiles: number of files to be stored in the tarball
 * filenames: array with path names of the files to be included in the tarball
 * tarname: name of the tarball archive
 * 
 * On success, it returns EXIT_SUCCESS; upon error it returns EXIT_FAILURE. 
 * (macros defined in stdlib.h).
 *
 * HINTS: First reserve room in the file to store the tarball header.
 * Move the file's position indicator to the data section (skip the header) and
 * dump the contents of the source files (one by one) in the tarball archive. 
 * At the same time, build the representation of the tarball header in memory.
 * Finally, rewind the file's position indicator, write the number of files as
 * well as the (file name,file size) pairs in the tar archive.
 *
 * Important reminder: to calculate the room needed for the header, a simple 
 * sizeof of stHeaderEntry will not work. Bear in mind that, on disk, file names * found in (name,size) pairs occupy strlen(name)+1 bytes.
 *
 */
int createTar(int nFiles, char *fileNames[], char tarName[]) {
	stHeaderEntry *header;
	int i, offset = 0, error;
	FILE *tarFile, *inputFile;

	header = malloc(sizeof(stHeaderEntry) * nFiles);
	
	tarFile = fopen(tarName, "w+");	
	
	// We skip the header
	for(i = 0; i < nFiles; ++i) {
		offset += strlen(fileNames[i]) + 1;
	}

	offset += sizeof(unsigned int) * (nFiles + 1);
	error = fseek(tarFile, offset, SEEK_SET);
	
	i = 0;
	while((!error) && (i < nFiles)) {
		inputFile = fopen(fileNames[i], "r");
		if(!ferror(inputFile)) {
			header[i].name = fileNames[i];
			header[i].size = copynFile(inputFile, tarFile, INT_MAX) + 1;
			fclose(inputFile);
			++i;
			if(header[i].size == -1) {
				error = 1;
			}
		}
		else {
			error = 1;
		}
	}
	
	rewind(tarFile); // We return to the start of the file

	if(!error) {	
		fwrite(&i, sizeof(int), 1, tarFile);
		i = 0;
		while((i < nFiles) && (!error)) {
			fwrite(header[i].name, strlen(header[i].name) + 1, 1, tarFile);
			fwrite(&(header[i].size), sizeof(unsigned int), 1, tarFile);	
			if(ferror(tarFile)) {
					error = 1;
			}
			++i;
			
		}
	}
	fclose(tarFile);
	free(header);

	if(error) {
		clearerr(inputFile);
		clearerr(tarFile);
		return EXIT_FAILURE;
	}
	else {
		return EXIT_SUCCESS; 
	}
}

/** Extract files stored in a tarball archive
 *
 * tarName: tarball's pathname
 *
 * On success, it returns EXIT_SUCCESS; upon error it returns EXIT_FAILURE. 
 * (macros defined in stdlib.h).
 *
 * HINTS: First load the tarball's header into memory.
 * After reading the header, the file position indicator will be located at the 
 * tarball's data section. By using information from the header -number of files * and (file name, file size) pairs--, extract files stored in the data section
 * of the tarball.
 *
 */
int extractTar(char tarName[]) {
	int nFiles, i, j, error;
	FILE *tarFile, *out;
	stHeaderEntry *header;

	tarFile = fopen(tarName, "r");
	if(readHeader(tarFile, &header, &nFiles) == EXIT_SUCCESS) {
		for(i = 0; i < nFiles; ++i) {
			out = fopen(header[i].name, "w+");
			for(j = 0; j < header[i].size - 1; ++j) {
				fputc(fgetc(tarFile), out);
				if(ferror(out)) {
					error = 1;
				}
			}
			fclose(out);
		}	
	}
	else {
		error = 1;
	}
	fclose(tarFile);
	free(header);

	if(error) {
		clearerr(tarFile);
		clearerr(out);
		return EXIT_FAILURE;
	}
	else {
		return EXIT_SUCCESS; 
	}
}
