#!/bin/bash

if ! test -e mytar ; then
	echo "mytar file doesn't exist, or is not on the current directory"
	exit -1
elif ! test -x mytar ; then
	echo "mytar file isn't executable, change it with chmod +x mytar"
	exit -1
else
	if test -d tmp ; then
		rm -r tmp
	fi
	mkdir tmp
	cd tmp
	file1=file1.txt
	echo "Hello world!" > $file1
	file2=file2.txt
	head /etc/passwd > $file2
	file3=file3.dat
	head --bytes=1024 /dev/urandom > $file3
	../mytar -cf filetar.mtar $file1 $file2 $file3
	mkdir out
	cp ../mytar ./out/
	cd out
	../../mytar -xf ../filetar.mtar
	if test -n "$(diff ../$file1 $file1 2>&1)" ; then
		echo "File $file1 hasn't passed the test"
		exit -1
	elif test -n "$(diff ../$file2 $file2 2>&1)" ; then
		echo "File $file2 hasn't passed the test"
		exit -1
	elif test -n "$(diff ../$file3 $file3 2>&1)" ; then
		echo "File $file3 hasn't passed the test"
		exit -1
	else
		echo "Success"
		exit 0
	fi
fi
