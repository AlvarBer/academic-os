#!/bin/bash

rm virtual-disk

./fs-fuse -t 2097152 -a virtual-disk -f "-d-s mount-point"
./script.sh
fusermount -u mount-point
./fs-fuse -m -a virtual-disk -f "d -s mount-point"
ls mount-point
