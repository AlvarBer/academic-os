#!/bin/sh

check () {
	./my-fsck virtual-disk > /dev/null 2> /dev/null

	if test $? -eq 0 ; then
		echo "Filesystem Check passed"
	else
		echo "Filesystem Check failed"
		return 1
	fi
	if test -z "$(diff $1 $2)" ; then
		echo "File $1 and file $2 are the same"
	else
		echo "File $1 and file $2 aren't the same"
		echo "$(diff $1 $2)"
		return 1
	fi
	if test $# -eq 4 ; then
		if test -z "$(diff $3 $3)" ; then
			echo "File $3 and file $4 are the same"
		else
			echo "File $3 and file $4 aren't the same"
			echo "$(diff $3 $4)"
			return 1
		fi
	fi
	return 0 
}

fuseLibFile=fuseLib.c
myFSFile=myFS.h
mountPoint=mount-point
thirdFile=thirdFile.txt

rm -rf temp
mkdir temp

cp src/$fuseLibFile src/$myFSFile $mountPoint
cp src/$fuseLibFile src/$myFSFile ./temp

check $mountPoint/$fuseLibFile ./temp/$fuseLibFile $mountPoint/$myFSFile ./temp/$myFSFile

truncate --size=-4K ./temp/$fuseLibFile ./temp/$myFSFile
truncate --size=-4K $mountPoint/$fuseLibFile $mountPoint/$myFSFile

check $mountPoint/$fuseLibFile ./temp/$fuseLibFile $mountPoint/$myFSFile ./temp/$myFSFile

echo "This is the third file" > $mountPoint/$thirdFile
echo "This is the third file" > ./temp/$thirdFile

check $mountPoint/$thirdFile ./temp/$thirdFile

truncate --size=8K ./temp/$myFSFile $mountPoint/$myFSFile

check $mountPoint/$myFSFile ./temp/$myFSFile
