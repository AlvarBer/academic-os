#!/bin/sh

EXECFOLDER="executables"
SRCFOLDER="src"
SCHEDEX="./schedsim"
GANTGENERATOR="./generate_gantt_chart"

make -C $SRCFOLDER
if test ! -x "$SRCFOLDER/$SCHEDEX" ; then
	chmod +x SRCFOLDER/$SCHEDEX
fi

rm *.log *.eps 2> /dev/null

$SRCFOLDER/$SCHEDEX $*

cd $EXECFOLDER

for file in $(find ../ -maxdepth 1 -type f -name "CPU_?.log")
do
	./generate_gantt_chart $file
done

for file in $(find ../ -maxdepth 1 -type f -name "CPU_?.eps")
do
	xdg-open $file
done