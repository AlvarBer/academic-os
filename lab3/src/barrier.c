#include "barrier.h"
#include <errno.h>

#ifdef POSIX_BARRIER

/* Wrapper functions to use pthread barriers */

int sys_barrier_init(sys_barrier_t* barrier, unsigned int nthreads) {
	return pthread_barrier_init(barrier,NULL,nthreads);
}

int sys_barrier_destroy(sys_barrier_t* barrier) {
	return pthread_barrier_destroy(barrier);
}

int sys_barrier_wait(sys_barrier_t *barrier) {
	return pthread_barrier_wait(barrier);
}

#else

/* Barrier initialization function */
int sys_barrier_init(sys_barrier_t *barrier, unsigned int nr_threads) {
	barrier->max_threads = nr_threads;
	barrier->cur_barrier = 0; // A barrier starts being even
	barrier->nr_threads_arrived[0] = 0;
	barrier->nr_threads_arrived[1] = 0;

	if (pthread_mutex_init(&barrier->mutex, NULL) != 0 || pthread_cond_init(&barrier->cond, NULL) != 0)
		printf("Failure to initialize mutex/conditional variable\n");

	return 0;
}

/* Destroy barrier resources */
int sys_barrier_destroy(sys_barrier_t *barrier) {
	pthread_mutex_destroy(&barrier->mutex);
	pthread_cond_destroy(&barrier->cond); 

	printf("Barrier as been destroyed\n"); // TODO: Delete this line

	return 0;
}

/* Main synchronization operation */
int sys_barrier_wait(sys_barrier_t *barrier) {
	pthread_mutex_lock(&barrier->mutex); // We adquire the lock and increments the nr_threads_arrived
	barrier->nr_threads_arrived[barrier->cur_barrier]++; // In the event this is not the last thread to arrive at the barrier, the thread must block in the condition variable
	//printf("Even Threads: %d Odd Threads: %d\n", barrier->nr_threads_arrived[0], barrier->nr_threads_arrived[1]); //TODO: Delete this line

	if (barrier->nr_threads_arrived[barrier->cur_barrier] < barrier->max_threads) {
		pthread_cond_wait(&barrier->cond, &barrier->mutex);
		pthread_mutex_unlock(&barrier->mutex);
	}
	else {
		barrier->nr_threads_arrived[barrier->cur_barrier] = 0;
		barrier->cur_barrier = !barrier->cur_barrier; // Change from even to odd or viceversa
		pthread_cond_broadcast(&barrier->cond);
		pthread_mutex_unlock(&barrier->mutex);
	}

	return 0;
}

#endif /* POSIX_BARRIER */