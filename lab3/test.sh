#!/bin/bash

EXECFOLDER="executables"
SRCFOLDER="src"
SCHEDEX="./schedsim"
GANTGENERATOR="./generate_gantt_chart"

# Make
make -C $SRCFOLDER
mv $SRCFOLDER/$SCHEDEX $EXECFOLDER
if test ! -x "$EXECFOLDER/$SCHEDEX" ; then
	chmod +x $EXECFOLDER/$SCHEDEX
fi

echo "Please introduce the input file"
read FILE
while test ! -f $FILE ; do
	echo "File doesn't exists or is not a regular file"
	echo "Please enter a valid input file"
	read FILE
done

echo "Please introduce the number of simulated CPU's (Maximum 8)"
read NCPU
while test $NCPU -ge 8 || test $NCPU -lt 1 ; do
	echo "You introduced more than 8 as the number of CPU's (Or a number inferior to 1)"
	echo "Please enter a number equal or less than 8"
	read NCPU
done

rm -rf results 2> /dev/null
mkdir results

cd $EXECFOLDER

for SCHEDALGORITHM in $($SCHEDEX -L) ; do
	if test $SCHEDALGORITHM != "Available" && test $SCHEDALGORITHM != "schedulers:" ; then
		echo "We are doing algorithm $SCHEDALGORITHM"
		for ((i=1 ; i<=$NCPU ; ++i)) ; do
			$SCHEDEX -s $SCHEDALGORITHM -n $i -i ../$FILE
			for LOGFILE in $(find . -maxdepth 1 -type f -name "CPU_?.log" -printf "%f\n")
			do
				$GANTGENERATOR $LOGFILE
				mv $LOGFILE ../results/$SCHEDALGORITHM-N$i-$LOGFILE
			done
			for CHARTFILE in $(find . -maxdepth 1 -type f -name "CPU_?.eps" -printf "%f\n")
			do
				mv $CHARTFILE ../results/$SCHEDALGORITHM-N$i-$CHARTFILE
			done
		done
	fi
done
