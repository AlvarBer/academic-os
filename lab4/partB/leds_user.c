#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

#include "leds_user.h"

static int file = 0; // Same as NULL
const char* path = "/dev/leds";

int main(int argc, char *argv[]) {
	int opt;
	char* previous;
	if (argc < 2) {
		fprintf(stderr, "Usage: userLeds -i [direct input] -p [concrete pattern] -b [binary input]\n");
		exit(EXIT_FAILURE);
	}
	else {
		while ((opt = getopt(argc, argv, "a:i:p:b:")) != -1) {
			switch (opt) {
				case 'i':
					sendMessage(optarg);
				break;
				case 'p':
					printf("Functionality still not implemented\n");
				break;
				case 'b':
					previous = binaryInput(optarg);
				break;
				case 'a':
					previous = binaryAdd(optarg);
				break;
				default:
					abort();
			}
		}
	}

	return 0;
}

void sendMessage(char* msg) {
	printf("Sending Message %s with length %lu\n", msg, strlen(msg));
	if (!file) {
		printf("File wasn't open\n");
		file = open(path, O_WRONLY | O_TRUNC);
		if (file == -1)
			printf("File couldn't be opened\n");
	}	

	if(write(file, msg, strlen(msg) + 1) != strlen(msg))
		printf("Not all bytes could be written\n");

	close(file);
}

char* binaryInput(char* input) {
	char str[3] = "";
	if (strlen(input) != 3) {
		printf("Number is of length %lu, when it should be of length 3\n", strlen(input));
		return - 1;
	}

	if (input[0] == '1') {
		strcat(str, "1");
	}
	if (input[1] == '1') {
		strcat(str, "2");
	}
	if (input[2] == '1') {
		strcat(str, "3");
	}
		
	sendMessage(str);
	return input;
}

char* binaryAdd(char* previous) {
	printf("Previous input is: %s\n", previous);
	if (previous[2] == '0')
		previous[2] = '1';
	else {
		previous[2] = '0';
		if (previous[1] == '0')
			previous[1] = '1';
		else {
			previous[1] = '0';
			if (previous[0] == '0')
				previous[0] = '1';
			else {
				previous[0] = '0';
				previous[1] = '0';
				previous[2] = '0';
			}
		}
 
	}
	printf("Binary input is: %s\n", previous);
	previous = binaryInput(previous);
	
	return previous;
}
